/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import static Conexao.Conexao.getConnection;
import DTO.FuncionarioDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;



/**
 *
 * @author ElaineSantos
 */
public class FuncionarioDAO {
    
    public static ArrayList<FuncionarioDTO> getFuncionarios() {
        ArrayList<FuncionarioDTO> funcionarios = new ArrayList();
               
         try {
            Connection con = getConnection();
            PreparedStatement ps = con.prepareStatement("select * from funcionario");
            ResultSet rs = ps.executeQuery();
            
             while (rs.next()) {
                FuncionarioDTO a = new FuncionarioDTO();
                a.setId(rs.getInt(1));
                a.setNome(rs.getString(2));
                a.setIdade(rs.getInt(3));
                a.setTelefone(rs.getString(4));                
                funcionarios.add(a);                
            }
            
            ps.close();
            con.close();
        } catch (Exception e) {             
            e.printStackTrace();
        }
        return funcionarios;
    }    
    
    
}
