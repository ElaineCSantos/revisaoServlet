/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SERVLET;

import DAO.FuncionarioDAO;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



/**
 *
 * @author ElaineSantos
 */
public class ListarFuncionario extends HttpServlet{
    public String executar(HttpServletRequest request, HttpServletResponse response){
        FuncionarioDAO FuncionarioDAO = new FuncionarioDAO();
        
        request.setAttribute("funcionarios", FuncionarioDAO.getFuncionarios());

        return "/ListaFuncionario.jsp";
    }
}
