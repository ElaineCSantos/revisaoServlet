<%-- 
    Document   : ListaFuncionario
    Created on : 27/03/2017, 20:45:39
    Author     : ElaineSantos
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lista de Funcionarios</title>
        <style>
            table tr{
                text-align: left!important;
                
            }
            table, table tr td, table tr th{
                border: solid 1px;
            }
        </style>
    </head>
    <body>
        <h1>Funcionarios</h1>
        <table>
            <tr>
                <th>Id</th>                
                <th>Nome</th>
                <th>Idade</th>
                <th>Telefone</th>
            </tr>
        <c:forEach var="funcionarios" items="${funcionarios}">
            <tr>
                <td>
                         ${funcionarios.id}                   
                </td>
                <td>
                    ${funcionarios.nome}
                </td>
                <td>
                    ${funcionarios.idade}
                </td>
                <td>
                    ${funcionarios.telefone}
                </td>
            </tr>
            
        </c:forEach>
        </table>
        <br></br>
        <a href="index.html">Voltar</a>
        </body>
</html>

